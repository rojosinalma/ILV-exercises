# Call with:
# $> ruby solution.rb '{"employees":[{"firstName":"John", "lastName":"Doe"},{"firstName":"Anna", "lastName":"Smith"},{"firstName":"Peter", "lastName":"Jones"}]}'

require 'rubygems'
require 'json'
require 'awesome_print'

parsed_json = JSON.parse(ARGV[0],symbolize_names: true)
puts JSON.pretty_generate(parsed_json)
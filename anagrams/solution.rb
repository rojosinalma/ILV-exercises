require 'rubygems'

# For reference of this function name: http://www.azlyrics.com/lyrics/daftpunk/technologic.html
def technologic
  `nohup wget http://www.willmcgugan.com/files/WORD.zip -N -O ./words.zip &>  download.log` # Let's download this mofo
  `unzip -f ./words.zip` # Unzip it!

  # COMMANDMENT #2: THOU SHALL NOT CONSUME ALL THE MEMORY IN THE SYSTEM.
  # That's why we pre-save combinations in hash file for later use.
  words = Hash.new([])
  File.open("WORD.LST", "r") do |file|
    while line = file.gets
      word = line.chomp
      words[word.split('').sort!.join('')] += [word]
    end
    puts words
  end

  # If you wanna know the magic of the Module#Marshal: https://en.wikibooks.org/wiki/Ruby_Programming/Reference/Objects/Marshal
  File.open("word_hash", "w") do |file|
    Marshal.dump(words, file)
  end
end


# Because Picasso was obsessive with anagrams, get it ?
def picasso(queried_words = [])
  technologic unless File.exists? 'word_hash' # Lets not do all that again if we can avoid it, please.

  words = nil
  File.open("word_hash", "r") do |file|
    words = Marshal.load(file)
  end

  queried_words.each do |word|
    sorted_anagram = word.split('').sort!.join('')
    puts "#{word}: #{words[sorted_anagram].join(', ')}"
  end
end

queried_words = ARGV.to_a
picasso(queried_words)
For every item in ``bad_numbers`` we compare it with the items in ``numbers``, that means that
the solution is indeed in the scale of O(n+m) because the iteration scales linearly depending on the amount of items of both arrays.

Another user-friendly explantion would be that there's only one embeded iteration in the code.

$(document).ready(function(){
  $('#compare').click(function(){
    numbers = $('#numbers').val().split(',');
    bad_numbers = $('#bad_numbers').val().split(',');

    for (var i=0; i<bad_numbers.length; i++) {

      index = numbers.indexOf(bad_numbers[i]);
      if (index > -1) {
          numbers.splice(index, 1);
      }
    }

    result = ["Result: ", numbers].join(" ");
    $('.result').html(result);
  });
});